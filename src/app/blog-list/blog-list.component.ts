import { HttpBackend, HttpClient, HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-blog-list',
  templateUrl: './blog-list.component.html',
  styleUrls: ['./blog-list.component.css']
})
export class BlogListComponent implements OnInit {

  public blogList;

  constructor(public http: HttpClient, public router: Router) { }

  async ngOnInit() {
    this.blogList = await this.getList();
    console.log(this.blogList);
  }

  getList() {
    try {
      return this.http.get('https://docully.com/blog/api/blogs', { observe: 'response' }).toPromise().then((response: HttpResponse<any>) => {
        if (response.status === 200 || response.status === 201) {
          return Promise.resolve(response.body);
        }
      }).catch((error: HttpErrorResponse) => {
        return Promise.reject(error);
      })
    } catch (error) {
      console.log(error);
    }
  }

}
