import { HttpClient, HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-blog-detail',
  templateUrl: './blog-detail.component.html',
  styleUrls: ['./blog-detail.component.css']
})
export class BlogDetailComponent implements OnInit {

  public blogDetail;

  constructor(public http: HttpClient) { }

  async ngOnInit() {
    this.blogDetail = await this.getDetail();
    console.log(this.blogDetail);
  }

  getDetail() {
    try {
      return this.http.get('https://docully.com/blog/api/view/this-is-a-test-blog-2/blogs', { observe: 'response' }).toPromise().then((response: HttpResponse<any>) => {
        if (response.status === 200 || response.status === 201) {
          return Promise.resolve(response.body);
        }
      }).catch((error: HttpErrorResponse) => {
        return Promise.reject(error);
      })
    } catch (error) {
      console.log(error);
    }
  }

}
